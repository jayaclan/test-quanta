
interface CinemaProvide {
  logo_path: string
  provider_name: string
  provider_id: number
}

export default CinemaProvide