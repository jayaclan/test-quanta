interface Food {
    id: number
    title: string
    imagePath: any
    price: number
    
}

export default Food
