import { StyleSheet, Text, View, FlatList, TouchableOpacity, GestureResponderEvent } from "react-native";
import Movie from "../types/Movie";
import MovieCard from "./MovieCard";

interface MovieSectionProps {
  movies: Movie[];
  sectionTitle: string;
  viewAll: (event: GestureResponderEvent) => void;
}

function MovieSection({ movies, sectionTitle, viewAll }: MovieSectionProps) {
  return (
    <View style={{ height: 390, flexDirection: "column" }}>
      <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between",paddingHorizontal:20 }}>
        <Text style={styles.sectionTitle}>
          {sectionTitle}
        </Text>
        <TouchableOpacity onPress={(event) => viewAll(event)}>
          <Text style={{
            fontSize: 15,
            fontWeight: "400",
            color: "#BE179D"
          }}>
            {"View All"}
          </Text>
        </TouchableOpacity>
      </View>

      <FlatList
        style={{paddingHorizontal:20}}
        data={movies}
        renderItem={({ item }) =>
          <View style={styles.cardWraper}>
            <MovieCard
              key={item.id}
              movie={item}
              height={320}
              width={140}
            />
          </View>
        }
        horizontal={true}
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  sectionTitle: {
    fontSize: 22,
    fontWeight: "800",
    color: "#404040"
  },
  cardWraper: {
    marginRight: 10,
  }
});

export default MovieSection;
