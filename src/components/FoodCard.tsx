import { useState } from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity } from "react-native";
import Food from "../types/Food";

interface FoodCardProps {
  food: Food;
  height: number;
  width: number;
  imageSource: any;
}


function FoodCard({ food, height, width, imageSource }: FoodCardProps) {
  // Use a conditional to determine the correct image source
 

  return (
    <View style={{ marginVertical: 14, flexDirection: 'column' }}>
        <Image
            style={{
                alignItems: 'center',
                height, width,
                borderRadius: 20,
                borderWidth: 2,
                borderColor: "#252525",
            }}
            source={imageSource}
        />
        <Text numberOfLines={1} style={{
            marginTop: 8,
            color: "#2E2E2E",
            textAlign: "left",
            fontSize: 18,
            fontWeight: "700",
        }}>{food.title}</Text>

        <Text  style={{
            marginTop: 8,
            color: "#B70093",
            textAlign: "left",
            fontSize: 15,
            fontWeight: "700",
        }}>{`$ ${food.price}`}</Text>
    </View>
)	
}

export default FoodCard;
