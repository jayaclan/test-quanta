import { useState } from "react"
import { View, Modal, Text, Image, StyleSheet, TouchableOpacity } from "react-native"
import CinemaProvide from "../types/CinemaProvide"


function CinemaCard({ cinema, height,width }: { cinema: CinemaProvide, height: number ,width: number }) {
    const imageSrc = `https://image.tmdb.org/t/p/w500${cinema.logo_path}`


    return (
        <View style={{ margin: 14, height, flexDirection: 'row',backgroundColor:"#FFFFF" }}>
            <Image
                style={{
                    width,
                    borderRadius: 10,
                    borderWidth: 2,
                    borderColor: "#252525",
                }}
                resizeMode="contain"
                source={{ uri: imageSrc }}
            />
            <View style={{marginStart:12,flexDirection:"column"}}>
                <Text style={{
                    marginTop: 8,
                    color: "#2E2E2E",
                    textAlign: "left",
                    fontSize: 18,
                    fontWeight: "700",
                }}>{cinema.provider_name}</Text>
            </View>



        </View>
    )
}



export default CinemaCard
