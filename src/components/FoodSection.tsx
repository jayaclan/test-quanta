import { StyleSheet, Text, View, FlatList, TouchableOpacity, GestureResponderEvent } from "react-native";
import Movie from "../types/Movie";
import MovieCard from "./MovieCard";
import Food from "../types/Food";
import FoodCard from "./FoodCard";

interface FoodSectionProps {
  food: Food[];
  sectionTitle: string;
  viewAll: (event: GestureResponderEvent) => void;
}

function FoodSection({ food: movies, sectionTitle, viewAll }: FoodSectionProps) {
  return (
    <View style={{ height: null, flexDirection: "column" }}>
      <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between",paddingHorizontal:20 }}>
        <Text style={styles.sectionTitle}>
          {sectionTitle}
        </Text>
        <TouchableOpacity onPress={(event) => viewAll(event)}>
          <Text style={{
            fontSize: 15,
            fontWeight: "400",
            color: "#BE179D"
          }}>
            {"View All"}
          </Text>
        </TouchableOpacity>
      </View>

      <FlatList
        style={{paddingHorizontal:20}}
        data={movies}
        renderItem={({ item }) =>
          <View style={styles.cardWraper}>
            <FoodCard
              key={item.id}
              food={item}
              height={140}
              width={140}
              imageSource={item.imagePath}
            />
          </View>
        }
        horizontal={true}
        showsHorizontalScrollIndicator={false}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  sectionTitle: {
    fontSize: 22,
    fontWeight: "800",
    color: "#404040"
  },
  cardWraper: {
    marginRight: 10,
  }
});

export default FoodSection;
