import React from "react";
import { Text, View } from "react-native";
import { Ionicons } from "@expo/vector-icons";

interface TabIconProps {
    focused: boolean;
    color: string;
    size: number;
    title: string;
    iconName: "ios-home" | "ios-logo-apple" | "ios-navigate" | "ios-fast-food" | "ios-person";
}

const TabIcon: React.FC<TabIconProps> = ({iconName, focused, color, size, title }) => (
    <View style={{ flexDirection: "column", alignItems: "center" }}>
        <Ionicons
            name={iconName}
            size={focused ? size * 1.3 : size}
            color={color}
        />
        <Text style={{ color: color ,fontSize:11}}>{title}</Text>
    </View>
);

export default TabIcon;
