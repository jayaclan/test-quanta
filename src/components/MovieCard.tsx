import { useState } from "react"
import { View, Modal, Text, Image, StyleSheet, TouchableOpacity } from "react-native"

import Movie from "../types/Movie"
import MovieInfoModal from "./MovieInfoModal"

function MovieCard({ movie, height, width }: { movie: Movie, height: number, width: number }) {
    const imageSrc = `https://image.tmdb.org/t/p/w500${movie.poster_path}`
    const [visible, setVisible] = useState(false)

    const openModal = () => setVisible(true)
    const closeModal = () => setVisible(false)

    return (
        <>
            <Modal
                visible={visible}
                transparent={true}
                onRequestClose={closeModal}
                animationType="slide"
            >
                <TouchableOpacity onPress={closeModal} style={{ height: "100%" }} />
                <MovieInfoModal movie={movie} />
            </Modal>

            <View style={{ marginVertical: 14, height, width, flexDirection: 'column' }}>
                <Image
                    style={{
                        flex: 1,
                        borderRadius: 10,
                        borderWidth: 2,
                        borderColor: "#252525",
                    }}
                    source={{ uri: imageSrc }}
                />
                <Text style={{
                    marginTop: 8,
                    color: "#2E2E2E",
                    textAlign: "left",
                    fontSize: 18,
                    fontWeight: "700",
                }}>{movie.title}</Text>

                <TouchableOpacity onPress={openModal}>
                    <Text style={{
                        marginTop: 8,
                        paddingVertical: 8,
                        color: "#BE179D",
                        borderRadius: 20,
                        borderWidth: 2,
                        borderColor: "#BE179D",
                        textAlign: "center",
                        fontSize: 18,
                        fontWeight: "700",
                        paddingHorizontal: 12
                    }}>{"View Detail"}</Text>
                </TouchableOpacity>
            </View>
        </>
    )
}



export default MovieCard
