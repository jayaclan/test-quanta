import { Dimensions } from 'react-native'


const { height, width } = Dimensions.get('screen')
const tabBarHeight = 54

const APP_CONSTANT = {
  // api_base_url: Config.API_ENDPOINT,
  api_base_url: 'https://api.themoviedb.org/3',
  // api_key: `api_key=${Config.API_KEY}`,
  api_key: `5ee871172eb3b1e62eac67a0d7fc81c3`,
  api_read: `eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI1ZWU4NzExNzJlYjNiMWU2MmVhYzY3YTBkN2ZjODFjMyIsInN1YiI6IjYyMTQ3MGRiODEzODMxMDA2ZTEyNGYyMCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.Zw0QLozobLaafTZrclyv4ZEuqt0GLdRxNjqbuMAOtxQ`,
  goldenRatio: 1.618,
  width,
  height: height - tabBarHeight * 2
}

export default APP_CONSTANT
