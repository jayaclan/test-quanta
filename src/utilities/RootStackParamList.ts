import { RouteProp } from "@react-navigation/native";
import Movie from "../types/Movie";
import Food from "../types/Food";

export type RootStackParamList = {
  DetailScreen: { movies: Movie[], title: string };
  DetailFoodScreen: { food: Food[], title: string };
};

export type DetailScreenRouteProp = RouteProp<RootStackParamList, 'DetailScreen'>;


export type DetailFoodScreenRouteProp = RouteProp<RootStackParamList, 'DetailFoodScreen'>;