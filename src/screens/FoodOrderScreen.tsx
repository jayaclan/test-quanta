import { useEffect, useState } from "react"
import { ScrollView, StyleSheet, Image, View, Text } from "react-native"
import { Foundation as FoundationIcon } from "@expo/vector-icons"
import { Ionicons } from "@expo/vector-icons"
import Movie from "../types/Movie"
import MovieSection from "../components/MovieSection"
import APP_CONSTANT from "../utilities/constants"
import FoodSection from "../components/FoodSection"
import Food from "../types/Food"
import { useNavigation } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"
import { RootStackParamList } from "../utilities/RootStackParamList"

function FoodOrderScreen() {
    const [topSnack, setNowTopSnack] = useState<Food[]>([])
    const [meals, setNowMeals] = useState<Food[]>([])
    const navigation = useNavigation<StackNavigationProp<RootStackParamList>>();
    const controller = new AbortController()

    useEffect(() => {
        getData()
    }, [])

    const getData = () => {
        const dummytopSnack: Food[] = [
            {
                id: 1,
                title: 'Pop corn',
                imagePath: require("../assets/popcorn.png"),
                price: 12.99,
            },
            {
                id: 2,
                title: 'French Fries',
                imagePath: require("../assets/fries.png"),
                price: 9.99,
            },
            {
                id: 3,
                title: 'Quanta Soda Drink',
                imagePath: require("../assets/cola.png"),
                price: 15.99,
            },
            {
                id: 4,
                title: 'Pop corn',
                imagePath: require("../assets/popcorn.png"),
                price: 12.99,
            },
            {
                id: 5,
                title: 'French Fries',
                imagePath: require("../assets/fries.png"),
                price: 9.99,
            },
            {
                id: 6,
                title: 'Quanta Soda Drink',
                imagePath: require("../assets/cola.png"),
                price: 15.99,
            },
            {
                id: 7,
                title: 'Quanta Soda Drink',
                imagePath: require("../assets/cola.png"),
                price: 15.99,
            },
            {
                id: 8,
                title: 'Quanta Soda Drink',
                imagePath: require("../assets/cola.png"),
                price: 15.99,
            },
            {
                id: 9,
                title: 'Quanta Soda Drink',
                imagePath: require("../assets/cola.png"),
                price: 15.99,
            },
            {
                id: 15,
                title: 'French Fries',
                imagePath: require("../assets/fries.png"),
                price: 9.99,
            },

        ];

        setNowTopSnack(dummytopSnack)

        const dummyMeals: Food[] = [
            {
                id: 1,
                title: 'Burger Deluxe',
                imagePath: require("../assets/burger.png"),
                price: 12.99,
            },
            {
                id: 2,
                title: 'Quanta Hotdog',
                imagePath: require("../assets/hotdog.png"),
                price: 9.99,
            },
            {
                id: 3,
                title: 'Iqlis Taco',
                imagePath: require("../assets/taco.png"),
                price: 15.99,
            },
            {
                id: 4,
                title: 'Burger Deluxe',
                imagePath: require("../assets/burger.png"),
                price: 12.99,
            },
            {
                id: 5,
                title: 'Quanta Hotdog',
                imagePath: require("../assets/hotdog.png"),
                price: 9.99,
            },
            {
                id: 6,
                title: 'Iqlis Taco',
                imagePath: require("../assets/taco.png"),
                price: 15.99,
            },

        ];

        setNowMeals(dummyMeals)
    }

    return (
        <View style={{
            flex: 1,
            backgroundColor: "#FFFFFF",
            alignItems: "center",
        }}>
            <View style={{
                width: "100%",
                flexDirection: "column"
            }}>
                <View style={{
                    height: 50, backgroundColor: "#B70093", borderBottomEndRadius: 20, borderBottomStartRadius: 20
                    , marginBottom: 8
                }}>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", marginHorizontal: 20 }}>
                    <Ionicons
                        name="ios-search"
                        size={30}
                        color={"#FFFFFF"}
                    />
                    <Text style={{
                        color: "#404040",
                        textAlign: "center",
                        fontSize: 24,
                        fontWeight: "700",
                    }}>{"Quanta Snack Louis"}</Text>
                    <Ionicons
                        name="ios-search"
                        size={30}
                        color={"#404040"}
                    />
                </View>
                <ScrollView style={{
                    // marginHorizontal: 20,
                    marginBottom: 150
                }}>
                    <View style={{ marginHorizontal: 20 }}>
                        <Image
                            resizeMode="contain"
                            source={require("../assets/foodorder.png")}
                            style={{ width: "100%", height: 211 }}
                        />
                    </View>

                    <FoodSection
                        food={topSnack}
                        sectionTitle="Top Snack"
                        viewAll={(event) => {
                            navigation.navigate('DetailFoodScreen', { food: topSnack, title: "Top Snack" });
                        }}
                    />
                    <FoodSection
                        food={meals}
                        sectionTitle="Meals"
                        viewAll={(event) => {
                            navigation.navigate('DetailFoodScreen', { food: meals, title: "Meals" });
                        }}
                    />
                </ScrollView>
            </View>
        </View >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
        alignItems: "center",
    },
    scrollView: {
        marginLeft: 20,
        bottom: "20%",
        marginTop: 170
    },
    mainContent: {
        width: "100%",
        top: 50,
    },
    image: {
        alignItems: "center",
        marginBottom: 20,
    }
})

export default FoodOrderScreen 
