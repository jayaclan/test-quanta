import { useEffect, useState } from "react"
import { ScrollView, StyleSheet, Image, View, Text } from "react-native"
import { Foundation as FoundationIcon } from "@expo/vector-icons"
import { Ionicons } from "@expo/vector-icons"

import Movie from "../types/Movie"
import MovieSection from "../components/MovieSection"
import APP_CONSTANT from "../utilities/constants"
import { useNavigation } from "@react-navigation/native"
import { StackNavigationProp } from "@react-navigation/stack"
import { RootStackParamList } from "../utilities/RootStackParamList"

function HomeScreen() {
    const [nowPlayingMovies, setNowPlayingMovies] = useState<Movie[]>([])
    const [pouplarMovies, setPopularMovies] = useState<Movie[]>([])
    const controller = new AbortController()
    const navigation = useNavigation<StackNavigationProp<RootStackParamList>>();

    useEffect(() => {
        getData()
    }, [])

    const getData = () => {
        fetch("https://api.themoviedb.org/3/movie/now_playing?"
            + new URLSearchParams({ api_key: APP_CONSTANT.api_key }),
            { method: "GET", signal: controller.signal })
            .then(response => response.json())
            .then(data => {

                setNowPlayingMovies(data.results)
            })
            .catch()

        fetch("https://api.themoviedb.org/3/movie/popular?"
            + new URLSearchParams({ api_key: APP_CONSTANT.api_key }),
            { method: "GET", signal: controller.signal })
            .then(response => response.json())
            .then(data => setPopularMovies(data.results))
            .catch()
    }

    return (
        <View style={{
            flex: 1,
            backgroundColor: "#FFFFFF",
            alignItems: "center",
        }}>
            <View style={{
                width: "100%",
                flexDirection: "column"
            }}>
                <View style={{
                    height: 50, backgroundColor: "#B70093", borderBottomEndRadius: 20, borderBottomStartRadius: 20
                    , marginBottom: 8
                }}>

                </View>
                <ScrollView style={{
                    
                    marginBottom:100    
                }}>
                    <View style={{ flexDirection: "row", marginBottom: 20, alignItems: "center" ,marginHorizontal: 20,}}
                    >
                        <Image
                            source={{ uri: "https://newronanima.com/wp-content/uploads/2023/06/anime-jepang.jpg" }}
                            style={{ width: 50, height: 50, borderRadius: 25 }}
                            resizeMode="cover"
                        />
                        <View style={styles.columnTitle}>
                            <Text style={styles.textAlt}>{"Hay, Louis"}</Text>
                            <Text style={styles.textDesc}>{"Good Morning"}</Text>
                        </View>

                        <Ionicons
                            name="ios-notifications"
                            size={30}
                            color={"#404040"}
                        />
                    </View>

                    <Image
                        resizeMode="contain"
                        source={require("../assets/newuser.png")}
                        style={{ width: "100%", height: 211 }}
                    />

                    <MovieSection
                        movies={nowPlayingMovies}
                        sectionTitle="Recomendation"
                        viewAll={(event) => {
                            navigation.navigate('DetailScreen', { movies: nowPlayingMovies, title: "Recomendation" });
                        }}
                    />

                    <MovieSection
                        movies={pouplarMovies}
                        sectionTitle="UpComing"
                        viewAll={(event) => {
                            navigation.navigate('DetailScreen', { movies: nowPlayingMovies, title: "UpComing" });
                        }}
                    />
                </ScrollView>
            </View>
        </View >
    )
}

const styles = StyleSheet.create({

    columnTitle: {
        flexDirection: "column",
        flex: 1,
        marginLeft: 12
    },
    textAlt: {
        color: "#404040",
        textAlign: "left",
        fontSize: 22,
        fontWeight: "400",
    },
    textDesc: {
        color: "grey",
        textAlign: "left",
        fontSize: 14,
        fontWeight: "400",
    },
})

export default HomeScreen 
