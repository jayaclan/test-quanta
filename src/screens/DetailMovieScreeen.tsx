import React, { useEffect } from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";

import Movie from "../types/Movie";
import { DetailScreenRouteProp } from "../utilities/RootStackParamList";
import MovieCard from "../components/MovieCard";
import { useNavigation } from "@react-navigation/native";

interface DetailScreenProps {
  route: DetailScreenRouteProp;
}

const DetailMovieScreeen: React.FC<DetailScreenProps> = ({ route }) => {
  const { movies, title } = route.params;
  const navigation = useNavigation();

  useEffect(() => {
    // Update the route name dynamically based on the 'title' parameter
    navigation.setOptions({
      title: title,
    });
  }, [navigation, title]);
  return (
    <View style={styles.container}>
      <FlatList
        data={movies}
        renderItem={({ item }) => (
          <View style={{ flex: 1, margin: 5 }}>
            <MovieCard
              key={item.id}
              movie={item}
              height={320}
              width={null} // Set width to null to allow it to take full width
            />
          </View>
        )}
        keyExtractor={(item) => item.id.toString()}
        numColumns={2}
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: "#FFFFFF",
    marginBottom:40    
  },
});

export default DetailMovieScreeen;
