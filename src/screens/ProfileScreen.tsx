import { useEffect, useState } from "react"
import { ScrollView, StyleSheet, Image, View, Text, Switch } from "react-native"
import { Foundation as FoundationIcon } from "@expo/vector-icons"

import Movie from "../types/Movie"
import MovieSection from "../components/MovieSection"
import APP_CONSTANT from "../utilities/constants"
import { Avatar, ListItem } from "react-native-elements"
import BaseIcon from "../components/Icon"
import Chevron from "../components/Chevron"
import InfoText from "../components/InfoText"

function ProfileScreen() {

    return (
        <View style={{
            width: "100%",
            flexDirection: "column",
        }}>
            <View style={{
                height: 50, backgroundColor: "#B70093", borderBottomEndRadius: 20, borderBottomStartRadius: 20
                , marginBottom: 8
            }}></View>
            <ScrollView style={styles.scroll}>
                <View style={styles.userRow}>
                    <View style={styles.userImage}>
                        <Image
                            source={{ uri: "https://newronanima.com/wp-content/uploads/2023/06/anime-jepang.jpg" }}
                            style={{ width: 50, height: 50, borderRadius: 25 }}
                            resizeMode="cover"
                        />
                    </View>
                    <View>
                        <Text style={{ fontSize: 16 }}>{"Lousi Rudy Valen"}</Text>
                        <Text
                            style={{
                                color: 'gray',
                                fontSize: 16,
                            }}
                        >
                            {"Lrv1302@gmail.cm"}
                        </Text>
                    </View>
                </View>
                <InfoText text="More" />
                <View>
                    <ListItem
                        title="About US"
                        onPress={() => {}}
                        containerStyle={styles.listItemContainer}
                        leftIcon={
                            <BaseIcon
                                containerStyle={{ backgroundColor: '#B70093' }}
                                icon={{
                                    type: 'ionicon',
                                    name: 'md-information-circle',
                                }}
                            />
                        }
                        rightIcon={<Chevron />}
                    />
                    <ListItem
                        title="Terms and Policies"
                        onPress={() => {}}
                        containerStyle={styles.listItemContainer}
                        leftIcon={
                            <BaseIcon
                                containerStyle={{ backgroundColor: '#B70093' }}
                                icon={{
                                    type: 'entypo',
                                    name: 'light-bulb',
                                }}
                            />
                        }
                        rightIcon={<Chevron />}
                    />
                    <ListItem
                        title="Share our App"
                        onPress={() => {}}
                        containerStyle={styles.listItemContainer}
                        leftIcon={
                            <BaseIcon
                                containerStyle={{
                                    backgroundColor: '#B70093',
                                }}
                                icon={{
                                    type: 'entypo',
                                    name: 'share',
                                }}
                            />
                        }
                        rightIcon={<Chevron />}
                    />
                    <ListItem
                        title="Rate Us"
                        onPress={() => {}}
                        containerStyle={styles.listItemContainer}
                        leftIcon={
                            <BaseIcon
                                containerStyle={{
                                    backgroundColor: '#B70093',
                                }}
                                icon={{
                                    type: 'entypo',
                                    name: 'star',
                                }}
                            />
                        }
                        rightIcon={<Chevron />}
                    />
                    <ListItem
                        title="Send FeedBack"
                        onPress={() => {}}
                        containerStyle={styles.listItemContainer}
                        badge={{
                            value: 999,
                            textStyle: { fontSize: 14, color: 'white' },
                        }}
                        leftIcon={
                            <BaseIcon
                                containerStyle={{
                                    backgroundColor: '#B70093',
                                }}
                                icon={{
                                    type: 'materialicon',
                                    name: 'feedback',
                                }}
                            />
                        }
                        rightIcon={<Chevron />}
                    />
                </View>
            </ScrollView>
        </View>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
        alignItems: "center",
    },
    scrollView: {
        marginLeft: 20,
        bottom: "20%",
        marginTop: 170
    },
    mainContent: {
        width: "100%",
        top: 50,
    },
    image: {
        alignItems: "center",
        marginBottom: 20,
    },
    listItemContainer: {
        height: 55,
        borderWidth: 0.5,
        borderColor: '#ECECEC',
    },
    scroll: {
        backgroundColor: 'white',
    },
    userRow: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingBottom: 8,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 6,
    },
    userImage: {
        marginRight: 12,
    },
})

export default ProfileScreen 
