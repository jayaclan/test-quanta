import React, { useEffect } from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";

import Movie from "../types/Movie";
import { DetailFoodScreenRouteProp } from "../utilities/RootStackParamList";
import MovieCard from "../components/MovieCard";
import { useNavigation } from "@react-navigation/native";
import FoodCard from "../components/FoodCard";

interface DetailFoodScreenProps {
  route: DetailFoodScreenRouteProp;
}

const DetailFoodScreen: React.FC<DetailFoodScreenProps> = ({ route }) => {
  const { food: movies, title } = route.params;
  const navigation = useNavigation();

  useEffect(() => {
    // Update the route name dynamically based on the 'title' parameter
    navigation.setOptions({
      title: title,
    });
  }, [navigation, title]);
  return (
    <View style={styles.container}>
      <FlatList
        data={movies}
        renderItem={({ item }) => (
          <View style={{ flex: 1, margin: 5 }}>
            <FoodCard
              key={item.id}
              food={item}
              height={200}
              width={null}
              imageSource={item.imagePath}
            />
          </View>
        )}
        keyExtractor={(item) => item.id.toString()}
        numColumns={2}
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    backgroundColor: "#FFFFFF",
    marginBottom:40    
  },
});

export default DetailFoodScreen;
