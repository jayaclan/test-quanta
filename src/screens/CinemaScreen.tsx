import { useEffect, useState } from "react"
import { FlatList, Image, ScrollView, StyleSheet, Text, TextInput, View } from "react-native"
import { Ionicons } from "@expo/vector-icons"



import Movie from "../types/Movie"
import APP_CONSTANT from "../utilities/constants"
import CinemaProvide from "../types/CinemaProvide"
import CinemaCard from "../components/CinemaCard"

const tmdbLogo = require("../assets/tmdb-primary-logo.png")

function CinemaScreen() {
    const [cinema, setCinema] = useState<CinemaProvide[]>([])

    const controller = new AbortController()

    useEffect(() => {
        getData()
    }, [])
    const getData = () => {
        fetch('https://api.themoviedb.org/3/watch/providers/movie?language=en-US', {
            method: 'GET',
            headers: {
                accept: 'application/json',
                Authorization: `Bearer ${APP_CONSTANT.api_read}`
            }
        })
            .then(res => res.json())
            .then(json => {
                setCinema(json.results)
            })
            .catch(err => console.error('error:' + err));
    }


    return (
        <View style={{
            flex: 1,
            backgroundColor: "#FFFFFF",
            alignItems: "center",

        }}>
            <View style={{
                width: "100%",
                flexDirection: "column",
                marginHorizontal: 20
            }}>
                <View style={{
                    height: 50, backgroundColor: "#B70093", borderBottomEndRadius: 20, borderBottomStartRadius: 20
                    , marginBottom: 8
                }}>

                </View>
                <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", marginHorizontal: 20 }}>
                    <Ionicons
                        name="ios-search"
                        size={30}
                        color={"#FFFFFF"}
                    />
                    <Text style={{
                        color: "#404040",
                        textAlign: "center",
                        fontSize: 24,
                        fontWeight: "700",
                    }}>{"Cinema Provider"}</Text>
                    <Ionicons
                        name="ios-search"
                        size={30}
                        color={"#404040"}
                    />
                </View>
                <FlatList
                    data={cinema}
                    renderItem={({ item }) =>
                        <View style={{
                            marginRight: 10,
                        }}>
                            <CinemaCard
                                cinema={item}
                                height={100}
                                width = {140}
                            />
                        </View>
                    }
                    horizontal={false}
                    showsVerticalScrollIndicator={true}
                />

            </View>
        </View >
    )
}


export default CinemaScreen
