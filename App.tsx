import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from "@expo/vector-icons"
import { StatusBar } from 'expo-status-bar';
import { SafeAreaView, View } from 'react-native';
import TabIcon from './src/components/TabIconProps ';
import CinemaScreen from './src/screens/CinemaScreen';
import MyTicketScreen from './src/screens/MyTicketScreen';
import FoodOrderScreen from './src/screens/FoodOrderScreen';
import ProfileScreen from './src/screens/ProfileScreen';
import HomeScreen from './src/screens/HomeScreen';
import DetailMovieScreeen from './src/screens/DetailMovieScreeen';
import DetailFoodScreen from './src/screens/DetailFoodScreen';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function HomeNavigator() {
    return (
        <Stack.Navigator >
            <Stack.Screen
                name="home"
                component={HomeScreen}
                options={{ headerShown: false }} // Hide the entire header
            />
            <Stack.Screen name="DetailScreen" component={DetailMovieScreeen} />
        </Stack.Navigator>
    );
}


function FoodNavigator() {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="food" // Change the screen name to avoid conflicts
                component={FoodOrderScreen} // Assuming FoodOrderScreen is the correct screen
                options={{ headerShown: false }}
            />
            <Stack.Screen name="DetailFoodScreen" component={DetailFoodScreen} />
        </Stack.Navigator>
    );
}

function TabNavigator() {
    return (
        <SafeAreaView style={{ flex: 1 }}>
            <StatusBar style="auto" />

            <View style={{ flex: 1, position: 'relative' }}>
                <View
                    style={{
                        position: 'absolute',
                        bottom: 0,
                        left: 0,
                        right: 0,
                        backgroundColor: '#F5F6F6',
                        borderTopLeftRadius: 30,
                        borderTopRightRadius: 30,
                        marginBottom: 12,
                    }}
                />

                <Tab.Navigator
                    initialRouteName="Home"
                    screenOptions={{
                        headerShown: false,
                        tabBarShowLabel: false,
                        tabBarActiveTintColor: '#B70093',
                        tabBarStyle: {
                            position: 'absolute',
                            borderTopWidth: 0,
                            flexDirection: 'row',
                            paddingHorizontal: 16,
                            flex: 0, // Set flex to 0 to allow content to determine height
                        },
                    }}
                >
                    <Tab.Screen
                        options={{
                            tabBarIcon: ({ focused, color, size }) => (
                                <TabIcon iconName="ios-home" focused={focused} color={color} size={size} title="Home" />
                            ),
                        }}
                        name="Home"
                        component={HomeNavigator} // Use the StackNavigator here
                    />

                    <Tab.Screen
                        options={{
                            tabBarIcon: ({ focused, color, size }) => (
                                <TabIcon iconName="ios-logo-apple" focused={focused} color={color} size={size} title="Cinema" />
                            ),
                        }}
                        name="Cinema"
                        component={CinemaScreen}
                    />

                    {/* <Tab.Screen
                        options={{
                            tabBarIcon: ({ focused, color, size }) => (
                                <View
                                    style={{
                                        backgroundColor: focused ? '#B70093' : '#FFDBF8',
                                        borderRadius: 100,
                                        padding: 10,
                                    }}
                                >
                                    <Ionicons
                                        name="ios-navigate"
                                        size={focused ? size * 1.3 : size}
                                        color={focused ? '#F5F6F6' : '#B70093'}
                                    />
                                </View>
                            ),
                        }}
                        name="My Ticket"
                        component={MyTicketScreen}
                    /> */}
                    <Tab.Screen
                        options={{
                            tabBarIcon: ({ focused, color, size }) => (
                                <TabIcon iconName="ios-fast-food" focused={focused} color={color} size={size} title="Food Order" />
                            ),
                        }}
                        name="Food"
                        component={FoodNavigator} // Use FoodNavigator here
                    />


                    <Tab.Screen
                        options={{
                            tabBarIcon: ({ focused, color, size }) => (
                                <TabIcon iconName="ios-person" focused={focused} color={color} size={size} title="Profile" />
                            ),
                        }}
                        name="Profile"
                        component={ProfileScreen}
                    />
                </Tab.Navigator>
            </View>
        </SafeAreaView>
    );
}

export default function App() {
    return (
        <NavigationContainer>
            <TabNavigator />
        </NavigationContainer>
    );
}


